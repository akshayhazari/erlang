-module('perm').
%-export([permutation/1,newtl/1,instwo/2,addelem/2]).
-export([permu/1,addelem/2,newtl/1,instwo/2,remdup/1,permutation/1]).
%-export([addelem/2,newtl/1,instwo/2,remdup/1]).
newtl ([]) -> [];
newtl ([L|Ls]) -> (Ls).

remdup(L) -> lists:usort(L).

addelem(X,Ys)-> lists:foldl(fun(Z,Y) -> [[X|Z]] ++ Y end,[],Ys).

instwo (X,[])->[[X]];
instwo (X,[Y|Ys]) -> remdup([[Y,X|Ys]]++(addelem(Y,instwo(X,Ys)))).

permu([])->[];
permu([X,Y]) -> [[Y,X],[X,Y]];
permu([X,Y|Ys])-> [[X,Y|Ys]]++instwo(X,[Y|Ys]).

permutation(Ls)->(lists:foldl(fun(X,Y)->addelem(X,permu(Ls--[X]))++Y end,[],Ls)).


